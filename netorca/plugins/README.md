# NetOrca Ansible Modules

This directory includes the NetOrca Ansible modules. These modules are used to interact with the NetOrca API.

```
└── plugins
    ├── get_change_intances
    ├── get_service_items
    ├── get_services
    ├── update_change_instances
    ├── get_service_items_dependant
    ├── get_deployed_items
    ├── get_charges
    └── update_charges
```

## Examples

### Get Services
```yaml
- name: Get all service items
  netautomate.netorca.get_service:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get LoadBalancer service
  netautomate.netorca.get_service:
    url: https://app.netorca.io
    api_key: <api key here>
    service_name: 'LoadBalancer'
  register: results
```

### Get Service Items
```yaml
- name: Get all service items
  netautomate.netorca.get_service_items:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get service items of firewall service
  netautomate.netorca.get_service_items:
    url: https://app.netorca.io
    api_key: <api key here>
    service_name: 'Firewall'
  register: results
```

### Get Change Instances
```yaml
- name: Get all change instances
  netautomate.netorca.get_change_instances:
    url: https://dev.netorca.io
    api_key: <api key here>
  register: results
```

### Update Change Instances
```yaml
- name: Update change instance
  netautomate.netorca.update_change_instances:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <change instance ID here>
    state: ACCEPTED
  register: results

- name: Update change instance
  netautomate.netorca.update_change_instances:
    url: https://app.netorca.io
    username: team_member_a
    password: super_secure_password_here
    id: <change instance ID here>
    state: COMPLETED
  register: results

- name: Update change instance with deployed item
  netautomate.netorca.update_change_instances:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <change instance ID here>
    state: ACCEPTED
    deployed_item:
      name: 'test'
  register: results
```

### Get Service Items Dependant
```yaml
- name: Get all service items Dependant
  netautomate.netorca.get_service_items_dependant:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get service items dependants of firewall service
  netautomate.netorca.get_service_items_dependant:
    url: https://app.netorca.io
    api_key: <api key here>
    service_name: 'firewall'
  register: results
```

### Get Deployed Items
```yaml
- name: Get all deployed items
  netautomate.netorca.get_deployed_items:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get deployed item
  netautomate.netorca.get_deployed_items:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <deployed item id here>
  register: results
```

### Get Charges
```yaml
- name: Get all charges
  netautomate.netorca.get_charges:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get charge with id 123
  netautomate.netorca.get_charges:
    url: https://app.netorca.io
    api_key: <api key here>
    id: 123
  register: results
```

### Update Charges
```yaml
- name: Update charges
  netautomate.netorca.update_charges:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <charge ID here>
    data: 
      key1: value1
      key2: value2
  register: results

- name: Update charges
  netautomate.netorca.update_charges:
    url: https://app.netorca.io
    username: team_member_a
    password: super_secure_password_here
    id: <charge ID here>
    data: 
      key1: value1
      key2: value2
  register: results
```
