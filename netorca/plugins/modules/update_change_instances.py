#!/usr/bin/env python3
import pdb

from ansible.module_utils.basic import AnsibleModule

from ..module_utils.netorca_common import NetorcaBaseModule, NetorcaModuleValidationError
from ..module_utils.netorca_constants import (
    FIELDS_API_KEY,
    FIELDS_CONTEXT,
    FIELDS_DEPLOYED_ITEM,
    FIELDS_OBJECT_ID,
    FIELDS_PASS,
    FIELDS_SERVICE,
    FIELDS_STATE,
    FIELDS_URL,
    FIELDS_USER,
    NETORCA_VALID_UPDATE_STATES,
)

DOCUMENTATION = r"""
---
module: netautomate.netorca.update_change_instances

short_description: Update Change Instances in NetOrca

version_added: "0.1.0"

description: This module updates the change instances for a team in NetOrca.

options:
    url:
        description: Base URL for NetOrca.
        required: true
        type: str
    api_key:
        description: API Key generated for the team. If this is not specified, username and password need to be provided.
        required: false
        type: str
    username:
        description: Username for the account in the team. Use when no API KEY is available.
        required: false
        type: str
    password:
        description: Password for the account in the team. Use when no API KEY is available.
        required: false
        type: str
    id:
        description: The ID of the change instance to update.
        required: true
        type: str
    state:
        description: One of 'PENDING', 'ACCEPTED', 'COMPLETED'.
        required: true
        type: str
    deployed_item:
        description: The deployed item to update.
        required: false
        type: dict

author:
    - Scott Rowlandson
"""

EXAMPLES = r"""
- name: Update change instance
  netautomate.netorca.update_change_instances:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <change instance ID here>
    state: ACCEPTED
  register: results

- name: Update change instance
  netautomate.netorca.update_change_instances:
    url: https://app.netorca.io
    username: team_member_a
    password: super_secure_password_here
    id: <change instance ID here>
    state: COMPLETED
  register: results

- name: Update change instance with deployed item
  netautomate.netorca.update_change_instances:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <change instance ID here>
    state: ACCEPTED
    deployed_item:
      name: 'test'
  register: results
"""

RETURN = r"""
# These are examples of possible return values, and in general, should use other names for return values.
updated_change_instances:
    description: The change instance returned by NetOrca.
    type: list
    returned: always
message:
    description: A general message, useful when errors are encountered.
    type: str
    returned: always
    sample: 'Updated change instance successfully'
"""


class UpdateChangeInstances(NetorcaBaseModule):
    RESULT_FIELD_SI = "updated_change_instances"

    def validate_params(self) -> None:
        super().validate_params()

        # Check that State is one of valid states
        if FIELDS_STATE in self.module.params:
            if not self.module.params[FIELDS_STATE] in NETORCA_VALID_UPDATE_STATES:
                raise NetorcaModuleValidationError("'state' is not one of {NETORCA_VALID_UPDATE_STATES}")

    def run_module(self, func: str, method: str, id: str = None, data: dict = None) -> None:
        """Execute the given function and handle any errors."""
        try:
            self.connect()
            filters = self.make_filter()
            context = self.get_context()
            result_list = self.netorca.caller(func, method, filters=filters, context=context, id=id, data=data)
            if method in ["update", "patch"]:
                if data[FIELDS_STATE] == result_list[FIELDS_STATE]:
                    self.output_dict[self.RESULT_FIELD_CHANGED] = True

            if isinstance(result_list, list):
                self.make_response(result_list, as_list=True)
            else:
                self.make_response(result_list)
        except Exception as e:
            self.fail_module(f"An error occurred: {str(e)}")


FIELDS = {
    FIELDS_URL: dict(type="str", required=True),
    FIELDS_API_KEY: dict(type="str", required=False, no_log=True),
    FIELDS_USER: dict(type="str", required=False),
    FIELDS_PASS: dict(type="str", required=False, no_log=True),
    FIELDS_SERVICE: dict(type="str", required=False),
    FIELDS_CONTEXT: dict(type="str", required=False),
    FIELDS_OBJECT_ID: dict(type="str", required=False),
    FIELDS_STATE: dict(type="str", required=False),
    FIELDS_DEPLOYED_ITEM: dict(type="dict", required=False, default=None),
}


def main() -> None:
    module = AnsibleModule(argument_spec=FIELDS, supports_check_mode=True)

    try:
        data = {FIELDS_STATE: module.params[FIELDS_STATE]}
        if FIELDS_DEPLOYED_ITEM in module.params and module.params[FIELDS_DEPLOYED_ITEM] is not None:
            data[FIELDS_DEPLOYED_ITEM] = module.params[FIELDS_DEPLOYED_ITEM]
        UpdateChangeInstances(module).run_module(
            "change_instances",
            "update",
            id=module.params[FIELDS_OBJECT_ID],
            data=data,
        )
    except Exception as e:
        module.fail_json(msg=f"An error occurred: {str(e)}")


if __name__ == "__main__":
    main()
