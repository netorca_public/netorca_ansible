#!/usr/bin/env python3
from ansible.module_utils.basic import AnsibleModule

from ..module_utils.netorca_common import NetorcaBaseModule
from ..module_utils.netorca_constants import (
    FIELDS_API_KEY,
    FIELDS_CONTEXT,
    FIELDS_FILTERS,
    FIELDS_OBJECT_ID,
    FIELDS_PASS,
    FIELDS_SERVICE,
    FIELDS_URL,
    FIELDS_USER,
)

DOCUMENTATION = r"""
---
module: netautomate.netorca.get_change_instances

short_description: Get Change Instances form NetOrca

version_added: "0.1.0"

description: This modules gets the change instances for a team in Netorca

options:
    url:
        description : Base URL for NetOrca.
        required: true
        type: str
    api_key:
        description : API Key generated for team. If this is not specified, username and password need to be provided.
        required: false
        type: str
    username:
        description : Username for account in the team. Use when no API KEY is available.
        required: false
        type: str
    password:
        description : Password for account in the team. Use when no API KEY is available.
        required: false
        type: str

author:
    - Scott Rowlandson
"""

EXAMPLES = r"""
- name: Get all change instances
  netautomate.netorca.get_change_instances:
    url: https://dev.netorca.io
    api_key: <api key here>
  register: results
"""

RETURN = r"""
# These are examples of possible return values, and in general should use other names for return values.
change_instances:
    description: An array of the change instances returned by NetOrca
    type: array
    returned: always
message:
    description: A general message, useful when errors are encountered
    type: str
    returned: always
    sample: 'Returned 10 change instances'
"""


class GetChangeInstances(NetorcaBaseModule):
    RESULT_FIELD_SI = "change_instances"


FIELDS = {
    FIELDS_URL: dict(type="str", required=True),
    FIELDS_API_KEY: dict(type="str", required=False, no_log=True),
    FIELDS_USER: dict(type="str", required=False),
    FIELDS_PASS: dict(type="str", required=False, no_log=True),
    FIELDS_SERVICE: dict(type="str", required=False),
    FIELDS_CONTEXT: dict(type="str", required=False),
    FIELDS_FILTERS: dict(type="dict", required=False),
    FIELDS_OBJECT_ID: dict(type="str", required=False),
}


def main() -> None:
    module = AnsibleModule(argument_spec=FIELDS, supports_check_mode=True)

    try:
        GetChangeInstances(module).run_module(
            "change_instances",
            "get",
            id=module.params.get(FIELDS_OBJECT_ID, None),
        )
    except Exception as e:
        module.fail_json(msg=f"An error occurred: {str(e)}")


if __name__ == "__main__":
    main()
