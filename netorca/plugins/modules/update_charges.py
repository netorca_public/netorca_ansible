#!/usr/bin/env python3
from ansible.module_utils.basic import AnsibleModule

from ..module_utils.netorca_common import NetorcaBaseModule, NetorcaModuleValidationError
from ..module_utils.netorca_constants import (
    FIELDS_API_KEY,
    FIELDS_CONTEXT,
    FIELDS_DATA,
    FIELDS_DEPLOYED_ITEM,
    FIELDS_OBJECT_ID,
    FIELDS_PASS,
    FIELDS_SERVICE,
    FIELDS_URL,
    FIELDS_USER,
)

DOCUMENTATION = r"""
---
module: netautomate.netorca.update_charges

short_description: Update Charges in NetOrca

version_added: "1.0.23"

description: This module updates the charges in NetOrca.

options:
    url:
        description: Base URL for NetOrca.
        required: true
        type: str
    api_key:
        description: API Key generated for the team. If this is not specified, username and password need to be provided.
        required: false
        type: str
    username:
        description: Username for the account in the team. Use when no API KEY is available.
        required: false
        type: str
    password:
        description: Password for the account in the team. Use when no API KEY is available.
        required: false
        type: str
    id:
        description: The ID of the change instance to update.
        required: true
        type: str
    data:
        description: A key:value dictionary with items to update.
        required: true
        type: dict


author:
    - Scott Rowlandson
"""

EXAMPLES = r"""
- name: Update charges
  netautomate.netorca.update_charges:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <charge ID here>
    data:
      key1: value1
      key2: value2
  register: results

- name: Update charges
  netautomate.netorca.update_charges:
    url: https://app.netorca.io
    username: team_member_a
    password: super_secure_password_here
    id: <charge ID here>
    data:
      key1: value1
      key2: value2
  register: results
"""

RETURN = r"""
# These are examples of possible return values, and in general, should use other names for return values.
updated_charges:
    description: The charges returned by NetOrca.
    type: list
    returned: always
message:
    description: A general message, useful when errors are encountered.
    type: str
    returned: always
    sample: 'Updated change instance successfully'
"""


class UpdateCharges(NetorcaBaseModule):
    RESULT_FIELD_SI = "updated_charges"

    def validate_params(self) -> None:
        super().validate_params()

        if FIELDS_DATA in self.module.params and not isinstance(self.module.params[FIELDS_DATA], dict):
            raise NetorcaModuleValidationError(f"'{FIELDS_DATA}' field must be a dictionary")

    def run_module(self, func: str, method: str, id: str = None, data: dict = None) -> None:
        """Execute the given function and handle any errors."""
        try:
            self.connect()
            filters = self.make_filter()
            context = self.get_context()
            result_list = self.netorca.caller(func, method, filters=filters, context=context, id=id, data=data)
            if isinstance(result_list, list):
                self.make_response(result_list, as_list=True)
            else:
                self.make_response(result_list)
        except Exception as e:
            self.fail_module(f"An error occurred: {str(e)}")


FIELDS = {
    FIELDS_URL: dict(type="str", required=True),
    FIELDS_API_KEY: dict(type="str", required=False, no_log=True),
    FIELDS_USER: dict(type="str", required=False),
    FIELDS_PASS: dict(type="str", required=False, no_log=True),
    FIELDS_SERVICE: dict(type="str", required=False),
    FIELDS_CONTEXT: dict(type="str", required=False),
    FIELDS_OBJECT_ID: dict(type="str", required=False),
    FIELDS_DATA: dict(type="dict", required=False),
}


def main() -> None:
    module = AnsibleModule(argument_spec=FIELDS, supports_check_mode=True)

    try:
        data = module.params[FIELDS_DATA]
        UpdateCharges(module).run_module(
            "charges",
            "patch",
            id=module.params[FIELDS_OBJECT_ID],
            data=data,
        )
    except Exception as e:
        module.fail_json(msg=f"An error occurred: {str(e)}")


if __name__ == "__main__":
    main()
