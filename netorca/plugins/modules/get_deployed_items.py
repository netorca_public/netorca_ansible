#!/usr/bin/env python3
from ansible.module_utils.basic import AnsibleModule

from ..module_utils.netorca_common import NetorcaBaseModule
from ..module_utils.netorca_constants import (
    FIELDS_API_KEY,
    FIELDS_CONTEXT,
    FIELDS_FILTERS,
    FIELDS_OBJECT_ID,
    FIELDS_PASS,
    FIELDS_SERVICE,
    FIELDS_URL,
    FIELDS_USER,
)

DOCUMENTATION = r"""
---
module: netautomate.netorca.get_deployed_items.yml

short_description: Get Deployed Items form NetOrca

version_added: "0.1.21"

description: This modules gets the deployed items for a team in Netorca

options:
    url:
        description : Base URL for NetOrca.
        required: true
        type: str
    api_key:
        description : API Key generated for team. If this is not specified, username and password need to be provided.
        required: false
        type: str
    username:
        description : Username for account in the team. Use when no API KEY is available.
        required: false
        type: str
    password:
        description : Password for account in the team. Use when no API KEY is available.
        required: false
        type: str

author:
    - Scott Rowlandson
"""

EXAMPLES = r"""
- name: Get all deployed items
  netautomate.netorca.get_deployed_items:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get deployed item
  netautomate.netorca.get_deployed_items:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <deployed item id here>
  register: results
"""

RETURN = r"""
# These are examples of possible return values, and in general should use other names for return values.
deployed_items:
    description: An array of all the instances of that deployed items
    type: array
    returned: always
message:
    description: A general message, useful when errors are encountered
    type: str
    returned: always
    sample: 'Returned 10 items'
"""


class GetDeployedItems(NetorcaBaseModule):
    RESULT_FIELD_SI = "deployed_items"


FIELDS = {
    FIELDS_URL: dict(type="str", required=True),
    FIELDS_API_KEY: dict(type="str", required=False, no_log=True),
    FIELDS_USER: dict(type="str", required=False),
    FIELDS_PASS: dict(type="str", required=False, no_log=True),
    FIELDS_CONTEXT: dict(type="str", required=False),
    FIELDS_FILTERS: dict(type="dict", required=False),
    FIELDS_OBJECT_ID: dict(type="str", required=False),
}


def main() -> None:
    module = AnsibleModule(argument_spec=FIELDS, supports_check_mode=True)

    try:
        GetDeployedItems(module).run_module("deployed_items", "get", id=module.params.get(FIELDS_OBJECT_ID, None))
    except Exception as e:
        module.fail_json(msg=f"An error occurred: {str(e)}")


if __name__ == "__main__":
    main()
