---

# NetOrca Ansible Modules

This repository contains Ansible modules for interacting with the NetOrca API. The modules allow you to perform various tasks related to NetOrca, such as retrieving service items and managing changes.

## Modules

- get_service_items
- get_services
- get_change_instances
- update_change_instances
- get_service_items_dependant
- get_deployed_items
- get_charges
- update_charges

## Installation

To use these modules, perform the following steps:

1. Install the `netorca` galaxy packages by running the following command:

   ```shell
   ansible-galaxy collection install netautomate.netorca
   ```

2. Copy the desired module(s) to your Ansible project's directory.

3. Use the modules in your Ansible playbooks as outlined in the module documentation.

## Dependencies

- `netorca_sdk` (Install using `pip install netorca_sdk`)
- `validators` (Install using `pip install validators`)

## Examples

### Get Services
```yaml
- name: Get all service items
  netautomate.netorca.get_service:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get LoadBalancer service
  netautomate.netorca.get_service:
    url: https://app.netorca.io
    api_key: <api key here>
    service_name: 'LoadBalancer'
  register: results
```

### Get Service Items
```yaml
- name: Get all service items
  netautomate.netorca.get_service_items:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get service items of firewall service
  netautomate.netorca.get_service_items:
    url: https://app.netorca.io
    api_key: <api key here>
    service_name: 'Firewall'
  register: results
```

### Get Change Instances
```yaml
- name: Get all change instances
  netautomate.netorca.get_change_instances:
    url: https://dev.netorca.io
    api_key: <api key here>
  register: results
```

### Update Change Instances
```yaml
- name: Update change instance
  netautomate.netorca.update_change_instances:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <change instance ID here>
    state: ACCEPTED
  register: results

- name: Update change instance
  netautomate.netorca.netorca_update_changes:
    url: https://app.netorca.io
    username: team_member_a
    password: super_secure_password_here
    id: <change instance ID here>
    state: COMPLETED
  register: results
```

### Get Service Items Dependant
```yaml
- name: Get all service items Dependant
  netautomate.netorca.get_service_items_dependant:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get service items dependants of firewall service
  netautomate.netorca.get_service_items_dependant:
    url: https://app.netorca.io
    api_key: <api key here>
    service_name: 'firewall'
  register: results
```

### Get Deployed Items
```yaml
- name: Get all deployed items
  netautomate.netorca.get_deployed_items:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get deployed item
  netautomate.netorca.get_deployed_items:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <deployed item id here>
  register: results
```

### Get Charges
```yaml
- name: Get all charges
  netautomate.netorca.get_charges:
    url: https://app.netorca.io
    api_key: <api key here>
  register: results

- name: Get charge with id 123
  netautomate.netorca.get_charges:
    url: https://app.netorca.io
    api_key: <api key here>
    id: 123
  register: results
```

### Update Charges
```yaml
- name: Update charges
  netautomate.netorca.update_charges:
    url: https://app.netorca.io
    api_key: <api key here>
    id: <charge ID here>
    data: 
      key1: value1
      key2: value2
  register: results

- name: Update charges
  netautomate.netorca.update_charges:
    url: https://app.netorca.io
    username: team_member_a
    password: super_secure_password_here
    id: <charge ID here>
    data: 
      key1: value1
      key2: value2
  register: results
```


## Contributing

Contributions to this repository are welcome! If you find any issues or have suggestions for improvement, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the GNU General Public License v3.0. See the [LICENSE](LICENSE) file for more details.

---
